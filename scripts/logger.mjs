export class LogView extends Application {
    constructor({firstUser}={}, options={}) {
        super(options);
        this.firstUser = firstUser;
    }

    async getData(options={}) {
        let response = {
            data: await this.firstUser.getFlag("lumberjack", "data")
        };

        response.empty = response.data.length == 0;

        return response;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            width: 900,
            height: 600,
            tabs: [],
            title: "Lumberjack",
            classes: ["lumberjack", "application", "logview"],
            template: `modules/lumberjack/templates/logview.html`,
            resizable: true,
            scrollY: ["log-container-flexcol"],
            submitOnChange: true,
            submitOnClose: true
        });
    }

    activateListeners(html) {
        super.activateListeners(html)

        let handleboi = (ev) => {
            this._controlHandler(ev,html)
        }
        html.find(".logitem-control").each((item, i) => {
            i.addEventListener("click", handleboi, false);
        });

    }

    /**
     * Handle what happens when someone clicks on a log control button
     * @param {event} - Some words to explain what this is
     * @param {html} - the jquery html object
     * @returns {Promise<void>}
     * @private
     */

    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();

        // Token Configuration
        buttons = [
            {
                label: "Clear Logs",
                class: 'clear-logs',
                icon: 'fas fa-trash-alt',
                onclick: (ev) => this._clearLogs(),
            },
            ...buttons,
        ];

        return buttons;
    }

    async _controlHandler(ev,html) {
        console.log("You clicked a button asshole!")
        console.log(ev)
        console.log(html)
        let data = await this._getLog(ev.target.dataset.logid)
        console.log(data)
        switch (ev.target.dataset.action) {
            case "revert-Update": await this._handleRevert(data);
        }
    }

    async _handleRevert(log) {
        console.group(`revert${log.document.type}_${log.document.id}`);
        let currentDoc = game.data[`${log.document.type.toLowerCase()}s`].find(x => x._id == log.document.id);
        if (!currentDoc) {
            console.log("Couldn't find Document to revert");
            return;
        }
        let data = duplicate(currentDoc);
        log.document.changes.forEach(change => {
            console.log(`Setting ${change.key} back to ${change.original}`);
            this.setPropertyValue(data, change.key, change.original);
        });

        switch (log.document.type) {
            case "Actor": await new Actor(currentDoc).update(data, { diff: true, revert: true }); break;
            case "Item": await new Item(currentDoc).update(data, { diff: true, revert: true  }); break;
            case "Combatant": await new Combatant(currentDoc).update(data, { diff: true, revert: true  }); break;
            case "JournalEntry": await new JournalEntry(currentDoc).update(data, { diff: true, revert: true  }); break;
            case "Scene": await new JournalEntry(currentDoc).update(data, { diff: true, revert: true  }); break;
            case "Macro": await new JournalEntry(currentDoc).update(data, { diff: true, revert: true  }); break;
            case "Setting": await new JournalEntry(currentDoc).update(data, { diff: true, revert: true  }); break;
        }
        let logs = this.firstUser.getFlag("lumberjack", "data");
        logs.find(x => x.id == log.id).reverted = true;
        this.firstUser.setFlag("lumberjack", "data", logs);
        this.render(true);
        console.groupEnd();
    }

    async _clearLogs() {
        await this.firstUser.setFlag("lumberjack", "data", []);
        this.render(true);
        console.log("Logs cleared");
    }

    async _getLog(id) {
        let data = await this.getData();
        return data.data.find(x => x.id == id);
    }

    // https://stackoverflow.com/a/18937118
    setPropertyValue(obj, propPath, value) {
        const [head, ...rest] = propPath.split('.');

        !rest.length
            ? obj[head] = value
            : this.setPropertyValue(obj[head], rest.join('.'), value);
    }

}
