import {LogView} from "./logger.mjs";

Hooks.once('init', function() {
    Handlebars.registerHelper('timestamp', function(unixtime) {
        let date = new Date(unixtime).toLocaleString()
        return (date)
    });
    Handlebars.registerHelper('ifeq', function(arg1, arg2, options) {
        return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
    });
    Handlebars.registerHelper('ifnot', function(arg1, arg2, options) {
        return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
    });
})

Hooks.once('ready', async function() {
    const firstUserId = Array.from(game.users.entries())[0][0]; // No one judge me for this code - Cody
    const firstUser = game.users.get(firstUserId);

    addMenu(firstUser);

    const documentTypes = [ "Actor", "Token", "Item", "Combatant", "JournalEntry", "Scene", "Macro", "Setting" ];
    for (let x in documentTypes) {
        const type = documentTypes[x];
        Hooks.on(`create${type}`, async function(document, options, userId) {
            handleCreate(firstUser, type, document, userId);
        });
        Hooks.on(`preUpdate${type}`, async function(document, change, options, userId) {
            handlePreUpdate(firstUser, type, document, change, options, userId);
        });
        Hooks.on(`preDelete${type}`, async function(document, options, userId) {
            handlePreDelete(firstUser, type, document, userId);
        });
    }
});

function handleCreate(firstUser, type, document, userId) {
    console.group(`preCreate${type}_${document.data._id}`);
    console.dir(document.data);
    let log = {
        id: foundry.utils.randomID(16),
        action: "Create",
        user: {
            id: userId,
            name: game.users.get(userId).data.name
        },
        document: {
            id: document.data._id,
            type: type,
            name: document.data.name ?? document.key,
            link: buildLink(type, document)
        },
        timestamp: Date.now()
    };
    let logs = firstUser.getFlag("lumberjack", "data");
    logs.push(log);
    firstUser.setFlag("lumberjack", "data", logs);
    rerenderApp();
    console.dir(log);
    console.groupEnd();
}

function handlePreUpdate(firstUser, type, document, change, options, userId) {
    console.group(`preUpdate${type}_${document.data._id}`);
    let reducedChange = duplicate(change);
    delete reducedChange._id;
    console.dir(document.data);
    console.dir(reducedChange);
    let log = {
        id: foundry.utils.randomID(16),
        action: options.revert ? "Revert" : "Update",
        user: {
            id: userId,
            name: game.users.get(userId).data.name
        },
        document: {
            id: document.data._id,
            type: type,
            name: document.data.name ?? document.key,
            changes: [],
            link: buildLink(type, document)
        },
        timestamp: Date.now()
    };
    for (let key in flattenObject(reducedChange)) {
        const original = getPropertyValue(document.data, key);
        const newValue = getPropertyValue(reducedChange, key);
        if ((!original && !newValue) || original == newValue) continue;
        console.log(`${key} was changed from ${original} to ${newValue}`);

        log.document.changes.push({key: key, original: original, updated: newValue});
    }
    let logs = firstUser.getFlag("lumberjack", "data");
    logs.push(log);
    firstUser.setFlag("lumberjack", "data", logs);
    rerenderApp();
    console.dir(log);
    console.groupEnd();
}

function handlePreDelete(firstUser, type, document, userId) {
    console.group(`preDelete${type}_${document.data._id}`);
    console.dir(document.data);
    let log = {
        id: foundry.utils.randomID(16),
        action: "Delete",
        user: {
            id: userId,
            name: game.users.get(userId).data.name
        },
        document: {
            id: document.data._id,
            type: type,
            name: document.data.name ?? document.key,
            original: document.data,
            link: buildLink(type, document)
        },
        timestamp: Date.now()
    };
    let logs = firstUser.getFlag("lumberjack", "data");
    logs.push(log);
    firstUser.setFlag("lumberjack", "data", logs);
    rerenderApp();

    console.dir(log);
    console.groupEnd();
}

function rerenderApp() {
    if (game.world.data.flags.lumberjack.app) {
        game.world.data.flags.lumberjack.app.render();
    }
}

function buildLink(type, document) {
    let checklist = ["Actor", "Item", "JournalEntry", "Scene", "Macro"]
    if (checklist.includes(type)) {
        let id = document.data._id;
        return TextEditor.enrichHTML(`@${type}[${id}]`, {
            secrets: false,
            entities: true,
            links: false,
            rolls: false,
            rollData: {}
        });
    }
    else return null
}

function addMenu(firstUser) {
    //Establish variables necessary for rendering
    if (!game.world.data.flags.lumberjack) {game.world.data.flags.lumberjack = {}}; // Intentionally not a user flag so it resets each launch
    if (!firstUser.getFlag("lumberjack", "data")) { firstUser.setFlag("lumberjack", "data", [])};
    let logView = game.world.data.flags.lumberjack?.app ?? undefined;
    const logButton = $('#logo');

    //Establish the calls to render the log application window
    logButton.click(ev => {
        ev.preventDefault();
        if (game.user.isGM) {
            if (!logView) {
                logView = game.world.data.flags.lumberjack.app = new LogView({ firstUser: firstUser });
            }
            if (logView.rendered) logvView.close()
            else logView.render(true)
        }
    })
}

// https://stackoverflow.com/a/53739792
function flattenObject(ob) {
    let toReturn = {};

    for (let i in ob) {
        if (!ob.hasOwnProperty(i)) continue;

        if ((typeof ob[i]) == 'object' && ob[i] !== null) {
            let flatObject = flattenObject(ob[i]);
            for (let x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) continue;

                toReturn[i + '.' + x] = flatObject[x];
            }
        } else {
            toReturn[i] = ob[i];
        }
    }
    return toReturn;
}

// https://stackoverflow.com/a/37510735
function getPropertyValue(obj1, dataToRetrieve) {
    return dataToRetrieve
        .split('.') // split string based on `.`
        .reduce(function(o, k) {
            return o && o[k]; // get inner property if `o` is defined else get `o` and return
        }, obj1) // set initial value as object
}

// https://stackoverflow.com/a/18937118
function setPropertyValue(obj, path, value) {
    var schema = obj;  // a moving reference to internal objects within obj
    var pList = path.split('.');
    var len = pList.length;
    for(var i = 0; i < len-1; i++) {
        var elem = pList[i];
        if( !schema[elem] ) schema[elem] = {}
        schema = schema[elem];
    }

    schema[pList[len-1]] = value;
}
